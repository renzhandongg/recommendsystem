export default {
    url: {
        basicUrl: process.env.NODE_ENV === 'development' ?
            'http://10.25.26.187:8004/' : 'http://192.168.29.131:8004/',
    },
    /**
     * @description 默认打开的首页的路由name值，默认为home
     */
    homeName: 'user',
};